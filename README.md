Esse é um relato de como estão sendo os encontros entre hackerudas e hackudas, mulheres que querem se apropriar de magias tecnologicamente digitais, analógicas e físicas

## Primeiro encontro
  Quem somos e o que queremos? Quais sãos as nossas expectativas?

## Segundo encontro
  Ficou tarefa, voltamos ao tema. Começamos a explorar palavras que não conhecemos. Troca cultural além de troca de sensações, como cada uma está se sentindo no dia.

    `coordenadas, espectro, sinal, distância entre equipamentos, espelhar conteúdo, diferença entre servidora e computadora normal`

